import QtQuick 2.15
import QtQuick.Controls 2.15
import org.kde.plasma.components 3.0 as PC3
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.13 as Kirigami

Item {
    height: PlasmaCore.Units.gridUnit
    width: timelineStuff.totalWidth

    Rectangle {
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            right: parent.right
        }
        height: 1
        color: Kirigami.Theme.textColor
    }

    Repeater {
        model: Math.ceil(timelineStuff.to / timelineStuff.tickDistance)
        Rectangle {
            height: PlasmaCore.Units.gridUnit / 2
            x: index * timelineStuff.frameWidth * timelineStuff.tickDistance
            y: height / 2
            width: 1
            color: Kirigami.Theme.textColor
            PC3.Label {
                text: index * timelineStuff.tickDistance
                anchors.bottom: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: 7
                opacity: 0.6
            }
            PC3.Label {
                text: (index * timelineStuff.tickDistance / timelineStuff.fps).toFixed(2).replace('.', ':')
                anchors.top: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: 7
                opacity: 0.6
            }
        }

    }

    Rectangle {
        height: PlasmaCore.Units.gridUnit / 2
        x: parent.width
        y: height / 2
        width: 1
        color: Kirigami.Theme.textColor
    }
}
