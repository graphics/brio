#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [YEAR] [YOUR NAME], [YOUR EMAIL]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Description of this extension goes here (if nicco will ever bother writing it)
"""

import inkex
import random
from time import sleep
from collections import defaultdict
from os.path import expanduser
from dataclasses import dataclass
from typing import Optional
from multiprocessing.connection import Client

prop_names = {'x': 'x', 'y': 'y', 'Width': 'width', 'Height': 'height',
             'Stroke': 'stroke', 'Stroke width': 'stroke-width',
             'Stroke opacity': 'stroke-opacity', 'Fill': 'fill',
             'Blur': None, 'Transform': 'transform',
             'Opacity': 'opacity', 'Fill opacity': 'fill-opacity',
             'Path': 'd'}

@dataclass
class Pathy:
    path: str
    def __repr__(self): return f'inkex.Path("{self.path}")'

def try_float(val):
    try: return float(val)
    except Exception: return None

builders = {'x': try_float, 'y': try_float, 'width': try_float, 'height': try_float,
            'd': inkex.Path, 'transform': inkex.transforms.Transform}

class NewBrioExtension(inkex.EffectExtension):

    def effect(self):

        props = ['x', 'y', 'width', 'height', 'd', 'transform']
        styles = ['stroke', 'stroke-width', 'stroke-opacity', 'fill', 'fill-opacity', 'opacity']
        defaults = {'stroke-opacity': 1, 'fill-opacity': 1, 'opacity': 1,
                    'transform': inkex.transforms.Transform()}

        conn = Client(('localhost', 6123), authkey=b'brio')
        to_check, output = conn.recv(), {}
        for name, properties in to_check.items():

            try: el = self.svg.getElementById(name)
            except Exception as e:
                output[name] = str(e)
                continue
            output[name] = {}

            for prop in properties:

                pname = prop_names[prop]

                if pname in props:
                    val = builders[pname](el.get(pname)) or defaults.get(pname)
                    if val is not None:
                        output[name][prop] = val
                elif pname in styles:
                    val = s[pname] if pname in (s := el.cascaded_style()) else defaults.get(pname)
                    if val:
                        output[name][prop] = inkex.styles.Style(f'{pname}:{val}')

        conn.send(output)
        conn.close()

if __name__ == '__main__':
    NewBrioExtension().run()
